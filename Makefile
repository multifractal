#
#   multifractal -- compute multifractal spectrum of 2D HDR images
#   Copyright (C) 2020  Claude Heiland-Allen
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

CXXSTDFLAGS = -std=c++17 -Wall -Wextra -pedantic -Wno-vla -Wno-narrowing -Wno-deprecated-copy
OPTFLAGS = -O3 -march=native

multifractal: multifractal.cc multifractal.cl.h exr.o
	g++ $(CXXSTDFLAGS) $(OPTFLAGS) -g -o $@ multifractal.cc exr.o -lOpenCL `pkg-config --libs OpenEXR`

exr.o: exr.cc
	g++ $(CXXSTDFLAGS) $(OPTFLAGS) -g -o $@ -c exr.cc `pkg-config --cflags OpenEXR`

%.cl.h: %.cl
	xxd -i < $< > $@

clean:
	-rm multifractal.cl.h exr.o

.PHONY: clean
