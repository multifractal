# multifractal

compute multifractal spectrum of 2D HDR images

<https://mathr.co.uk/multifractal>

<https://code.mathr.co.uk/multifractal>

## examples

[examples](examples)

## theory

### Wikipedia

<https://en.wikipedia.org/wiki/Multifractal_system>

> A multifractal system is a generalization of a fractal system in
> which a single exponent (the fractal dimension) is not enough to
> describe its dynamics; instead, a continuous spectrum of exponents
> (the so-called singularity spectrum) is needed.

### Chhabra1989

"Direct determination of the f(α) singularity spectrum"
by Ashvin Chhabra and Roderick V. Jensen
in Phys. Rev. Lett. 62, 1327 – Published 20 March 1989
<https://doi.org/10.1103/PhysRevLett.62.1327>

> The direct determination of the f(α) singularity spectrum from
> experimental data is a difficult problem. This Letter introduces a
> simple method for computing f(α) based on the theorems of Shannon,
> Eggelston, and Billingsley which is markedly superior to other
> recently proposed methods, especially when dealing with experimental
> data from low-dimensional chaotic systems where the underlying
> dynamics are unknown.

## build

    git clone https://code.mathr.co.uk/multifractal.git
    cd multifractal
    make

## usage

    ./multifractal platform resolution input.exr output.dat
    gnuplot -c ../multifractal.gnuplot "output.dat" \
      "q-dq.png" "q-dq-excerpt.png" "a-fa.png"

- platform is OpenCL platform index (eg 0)

- resultion is number of data points to compute (eg 1280)

- input.exr is input EXR image file (RGBA)

- output.dat is output data text

## legal

    multifractal -- compute multifractal spectrum of 2D HDR images
    Copyright (C) 2020  Claude Heiland-Allen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
