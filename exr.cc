/*
    multifractal -- compute multifractal spectrum of 2D HDR images
    Copyright (C) 2020  Claude Heiland-Allen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cstdlib>

#include <ImfNamespace.h>
#include <ImfInputFile.h>
#include <ImfHeader.h>
#include <ImfFrameBuffer.h>
#include <ImathBox.h>

namespace IMF = OPENEXR_IMF_NAMESPACE;
using namespace IMF;
using namespace IMATH_NAMESPACE;

extern "C" float *load_exr
  ( const char *filename
  , int *input_width
  , int *input_height
  )
{
  float *rgba = nullptr;
  try
  {
    InputFile file(filename);
    const Header &h = file.header();
    Box2i dw = h.dataWindow();
    int width = *input_width = dw.max.x - dw.min.x + 1;
    int height = *input_height = dw.max.y - dw.min.y + 1;
    rgba = (float *) std::malloc(sizeof(float) * 4 * width * height);
    FrameBuffer fb;
    fb.insert("R", Slice
      ( IMF::FLOAT
      , (char *) (&rgba[0] - dw.min.x - dw.min.y * width)
      , sizeof(float) * 4
      , sizeof(float) * 4 * width
      , 1
      , 1
      , 0.0f
      ));
    fb.insert("G", Slice
      ( IMF::FLOAT
      , (char *) (&rgba[1] - dw.min.x - dw.min.y * width)
      , sizeof(float) * 4
      , sizeof(float) * 4 * width
      , 1
      , 1
      , 0.0f
      ));
    fb.insert("B", Slice
      ( IMF::FLOAT
      , (char *) (&rgba[2] - dw.min.x - dw.min.y * width)
      , sizeof(float) * 4
      , sizeof(float) * 4 * width
      , 1
      , 1
      , 0.0f
      ));
    fb.insert("A", Slice
      ( IMF::FLOAT
      , (char *) (&rgba[3] - dw.min.x - dw.min.y * width)
      , sizeof(float) * 4
      , sizeof(float) * 4 * width
      , 1
      , 1
      , 1.0f
      ));
    file.setFrameBuffer(fb);
    file.readPixels(dw.min.y, dw.max.y);
    return rgba;
  }
  catch(...)
  {
    if (rgba)
    {
      std::free(rgba);
    }
    return nullptr;
  }
}
