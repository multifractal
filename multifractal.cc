/*
    multifractal -- compute multifractal spectrum of 2D HDR images
    Copyright (C) 2020  Claude Heiland-Allen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#define _DEFAULT_SOURCE

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tgmath.h>

#define CL_TARGET_OPENCL_VERSION 110
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#include <CL/cl.h>

static const
char multifactal_cl[] =
{
#include "multifractal.cl.h"
, 0
};

extern "C" float *load_exr(const char *filename, int *input_width, int *input_height);

static inline
const char *error_string(int err) {
  switch (err) {
  case CL_SUCCESS:                            return 0;
  case CL_DEVICE_NOT_FOUND:                   return "Device not found.";
  case CL_DEVICE_NOT_AVAILABLE:               return "Device not available";
  case CL_COMPILER_NOT_AVAILABLE:             return "Compiler not available";
  case CL_MEM_OBJECT_ALLOCATION_FAILURE:      return "Memory object allocation failure";
  case CL_OUT_OF_RESOURCES:                   return "Out of resources";
  case CL_OUT_OF_HOST_MEMORY:                 return "Out of host memory";
  case CL_PROFILING_INFO_NOT_AVAILABLE:       return "Profiling information not available";
  case CL_MEM_COPY_OVERLAP:                   return "Memory copy overlap";
  case CL_IMAGE_FORMAT_MISMATCH:              return "Image format mismatch";
  case CL_IMAGE_FORMAT_NOT_SUPPORTED:         return "Image format not supported";
  case CL_BUILD_PROGRAM_FAILURE:              return "Program build failure";
  case CL_MAP_FAILURE:                        return "Map failure";
  case CL_INVALID_VALUE:                      return "Invalid value";
  case CL_INVALID_DEVICE_TYPE:                return "Invalid device type";
  case CL_INVALID_PLATFORM:                   return "Invalid platform";
  case CL_INVALID_DEVICE:                     return "Invalid device";
  case CL_INVALID_CONTEXT:                    return "Invalid context";
  case CL_INVALID_QUEUE_PROPERTIES:           return "Invalid queue properties";
  case CL_INVALID_COMMAND_QUEUE:              return "Invalid command queue";
  case CL_INVALID_HOST_PTR:                   return "Invalid host pointer";
  case CL_INVALID_MEM_OBJECT:                 return "Invalid memory object";
  case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR:    return "Invalid image format descriptor";
  case CL_INVALID_IMAGE_SIZE:                 return "Invalid image size";
  case CL_INVALID_SAMPLER:                    return "Invalid sampler";
  case CL_INVALID_BINARY:                     return "Invalid binary";
  case CL_INVALID_BUILD_OPTIONS:              return "Invalid build options";
  case CL_INVALID_PROGRAM:                    return "Invalid program";
  case CL_INVALID_PROGRAM_EXECUTABLE:         return "Invalid program executable";
  case CL_INVALID_KERNEL_NAME:                return "Invalid kernel name";
  case CL_INVALID_KERNEL_DEFINITION:          return "Invalid kernel definition";
  case CL_INVALID_KERNEL:                     return "Invalid kernel";
  case CL_INVALID_ARG_INDEX:                  return "Invalid argument index";
  case CL_INVALID_ARG_VALUE:                  return "Invalid argument value";
  case CL_INVALID_ARG_SIZE:                   return "Invalid argument size";
  case CL_INVALID_KERNEL_ARGS:                return "Invalid kernel arguments";
  case CL_INVALID_WORK_DIMENSION:             return "Invalid work dimension";
  case CL_INVALID_WORK_GROUP_SIZE:            return "Invalid work group size";
  case CL_INVALID_WORK_ITEM_SIZE:             return "Invalid work item size";
  case CL_INVALID_GLOBAL_OFFSET:              return "Invalid global offset";
  case CL_INVALID_EVENT_WAIT_LIST:            return "Invalid event wait list";
  case CL_INVALID_EVENT:                      return "Invalid event";
  case CL_INVALID_OPERATION:                  return "Invalid operation";
  case CL_INVALID_GL_OBJECT:                  return "Invalid OpenGL object";
  case CL_INVALID_BUFFER_SIZE:                return "Invalid buffer size";
  case CL_INVALID_MIP_LEVEL:                  return "Invalid mip-map level";
  default: return "Unknown";
  }
}

static inline
void error_print(int err, int loc) {
  if (err == CL_SUCCESS) { return; }
  fprintf(stderr, "CL ERROR: %d %s (%d)\n", err, error_string(err), loc);
  abort();
}

#define E(err) error_print(err, __LINE__)

static inline
int max(int x, int y)
{
  return x > y ? x : y;
}

extern
int main(int argc, char **argv)
{
  const float Q_min = -10;
  const float Q_max = 10;

  // parse arguments
  if (! (argc > 4))
  {
    fprintf(stderr, "usage: %s platform resolution input.exr output.dat\n", argv[0]);
    abort();
  }
  const unsigned int PLATFORM = atoi(argv[1]);
  const int q_step = round(atoi(argv[2]) / round(Q_max - Q_min));
  const int q_resolution = q_step * round(Q_max - Q_min);
  const float Q_scale = round(Q_max - Q_min) / (float) q_resolution;
  const int q_zero = round((0 - Q_min) / Q_scale);
  const int q_one = round((1 - Q_min) / Q_scale);
  const char *input_filename = argv[3];
  const char *output_filename = argv[4];

  // load image
  int input_width = 0, input_height = 0;
  float *rgba = load_exr(input_filename, &input_width, &input_height);
  if (! rgba)
  {
    abort();
  }

  // enumerate OpenCL platforms
  cl_platform_id platform_id[64];
  cl_uint platform_ids;
  E(clGetPlatformIDs(64, &platform_id[0], &platform_ids));
  if (! (PLATFORM < platform_ids)) {
    fprintf(stderr, "invalid platform: %d (range is 0 to %d)\n", PLATFORM, platform_ids - 1);
    return 1;
  }
  for (cl_uint i = 0; i < platform_ids; ++i) {
    fprintf(stderr, "platform %d%s\n", i, i == PLATFORM ? " *** SELECTED ***" : "");
    char buf[1024];
    cl_platform_info info[5] =
      { CL_PLATFORM_PROFILE
      , CL_PLATFORM_VERSION
      , CL_PLATFORM_NAME
      , CL_PLATFORM_VENDOR
      , CL_PLATFORM_EXTENSIONS
      };
    for (int j = 0; j < 5; ++j) {
      buf[0] = 0;
      E(clGetPlatformInfo(platform_id[i], info[j], 1024, &buf[0], NULL));
      fprintf(stderr, "\t%s\n", buf);
    }
  }

  // create context
  cl_device_id device_id;
  E(clGetDeviceIDs(platform_id[PLATFORM], CL_DEVICE_TYPE_ALL, 1, &device_id, NULL));
  cl_context_properties properties[] =
    { CL_CONTEXT_PLATFORM, (cl_context_properties) platform_id[PLATFORM]
    , 0
    };
  cl_int err;
  cl_context context = clCreateContext(properties, 1, &device_id, NULL, NULL, &err);
  if (! context) { E(err); }
  cl_command_queue queue = clCreateCommandQueue(context, device_id, 0, &err);
  if (! queue) { E(err); }

  // compile program
  const char *sources[] = { multifactal_cl };
  cl_program program = clCreateProgramWithSource(context, 1, sources, NULL, &err);
  if (! program) { E(err); }
  err = clBuildProgram(program, 1, &device_id, "", NULL, NULL);
  if (err != CL_SUCCESS) {
    char *buf = (char *) malloc(1000000);
    buf[0] = 0;
    E(clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, 1000000, &buf[0], NULL));
    fprintf(stderr, "build failed:\n%s\n", buf);
    free(buf);
    E(err);
  }

  // create kernels
#define K(name) \
  cl_kernel name = clCreateKernel(program, #name, &err); \
  if (! name) { E(err); }
  K(k_mipmap_reduce)
  K(k_horizontal)
  K(k_vertical)
  K(k_regression)
  K(k_dim_q)
#undef K

  // compute mipmap count
  int levels;
  {
    int dim = max(input_width, input_height);
    for (levels = 1; dim > 1; levels++)
    {
      dim = (dim + 1) >> 1;
    }
  }

  // allocate images
  cl_mem
    image[levels], log_sum_P_q[levels], log_neg_sum_P_q_log_P[levels],
    num_f, num_a, fs, as, ds;
  {
    int width = input_width;
    int height = input_height;
    cl_image_format format = { CL_RGBA, CL_FLOAT };
    for (int level = 0; level < levels; ++level)
    {
      image[level] = clCreateImage2D
        (context, CL_MEM_READ_WRITE, &format, width, height, 0, NULL, &err);
      if (! image[level]) E(err);
      log_sum_P_q[level] = clCreateImage2D
        (context, CL_MEM_READ_WRITE, &format, q_resolution + 1, height, 0, NULL, &err);
      if (! log_sum_P_q[level]) E(err);
      log_neg_sum_P_q_log_P[level] = clCreateImage2D
        (context, CL_MEM_READ_WRITE, &format, q_resolution + 1, height, 0, NULL, &err);
      if (! log_neg_sum_P_q_log_P[level]) E(err);
      width = (width + 1) >> 1;
      height = (height + 1) >> 1;
    }
    num_f = clCreateImage2D
      (context, CL_MEM_READ_WRITE, &format, q_resolution + 1, levels, 0, NULL, &err);
    if (! num_f) E(err);
    num_a = clCreateImage2D
      (context, CL_MEM_READ_WRITE, &format, q_resolution + 1, levels, 0, NULL, &err);
    if (! num_a) E(err);
    fs = clCreateImage2D
      (context, CL_MEM_READ_WRITE, &format, q_resolution + 1, 1, 0, NULL, &err);
    if (! fs) E(err);
    as = clCreateImage2D
      (context, CL_MEM_READ_WRITE, &format, q_resolution + 1, 1, 0, NULL, &err);
    if (! as) E(err);
    ds = clCreateImage2D
      (context, CL_MEM_READ_WRITE, &format, q_resolution + 1, 1, 0, NULL, &err);
    if (! ds) E(err);
  }

  cl_event e_mipmap_reduce[levels];

  // upload image
  {
    size_t origin[3] = { 0, 0, 0 };
    size_t region[3] = { input_width, input_height, 1 };
    E(clEnqueueWriteImage
      ( queue, image[0], CL_TRUE, origin, region, 0, 0, rgba
      , 0, NULL, &e_mipmap_reduce[0]
      ));
    free(rgba);
  }

  // compute mipmap sum reductions
  {
    int width = input_width;
    int height = input_height;
    for (int level = 1; level < levels; ++level)
    {
      width = (width + 1) >> 1;
      height = (height + 1) >> 1;
      E(clSetKernelArg
        (k_mipmap_reduce, 0, sizeof(image[level - 1]), &image[level - 1]));
      E(clSetKernelArg
        (k_mipmap_reduce, 1, sizeof(image[level]), &image[level]));
      size_t global[2] = { width, height };
      E(clEnqueueNDRangeKernel
        ( queue, k_mipmap_reduce, 2, NULL, global, NULL
        , level, e_mipmap_reduce, &e_mipmap_reduce[level]
        ));
    }
  }

  // compute sums of powers and logs
  cl_event e_horizontal[levels];
  cl_event e_vertical[levels];
  {
    int height = input_height;
    for (int level = 0; level < levels; ++level)
    {
      {
        E(clSetKernelArg(k_horizontal, 0, sizeof(image[levels - 1]), &image[levels - 1]));
        E(clSetKernelArg(k_horizontal, 1, sizeof(image[level]), &image[level]));
        E(clSetKernelArg(k_horizontal, 2, sizeof(log_sum_P_q[level]), &log_sum_P_q[level]));
        E(clSetKernelArg(k_horizontal, 3, sizeof(log_neg_sum_P_q_log_P[level]), &log_neg_sum_P_q_log_P[level]));
        E(clSetKernelArg(k_horizontal, 4, sizeof(Q_min), &Q_min));
        E(clSetKernelArg(k_horizontal, 5, sizeof(Q_scale), &Q_scale));
        size_t global[2] = { q_resolution + 1, height };
        E(clEnqueueNDRangeKernel
          ( queue, k_horizontal, 2, NULL, global, NULL
          , levels, e_mipmap_reduce, &e_horizontal[level]
          ));
      }
      {
        E(clSetKernelArg(k_vertical, 0, sizeof(log_sum_P_q[level]), &log_sum_P_q[level]));
        E(clSetKernelArg(k_vertical, 1, sizeof(log_neg_sum_P_q_log_P[level]), &log_neg_sum_P_q_log_P[level]));
        E(clSetKernelArg(k_vertical, 2, sizeof(num_f), &num_f));
        E(clSetKernelArg(k_vertical, 3, sizeof(num_a), &num_a));
        E(clSetKernelArg(k_vertical, 4, sizeof(Q_min), &Q_min));
        E(clSetKernelArg(k_vertical, 5, sizeof(Q_scale), &Q_scale));
        E(clSetKernelArg(k_vertical, 6, sizeof(level), &level));
        size_t global = q_resolution + 1;
        E(clEnqueueNDRangeKernel
          ( queue, k_vertical, 1, NULL, &global, NULL
          , 1, &e_horizontal[level], &e_vertical[level]
          ));
      }
      height = (height + 1) >> 1;
    }
  }

  // simple linear regression for F
  cl_event e_slope_f;
  {
    E(clSetKernelArg(k_regression, 0, sizeof(num_f), &num_f));
    E(clSetKernelArg(k_regression, 1, sizeof(fs), &fs));
    size_t global = q_resolution + 1;
    E(clEnqueueNDRangeKernel
      ( queue, k_regression, 1, NULL, &global, NULL
      , levels, e_vertical, &e_slope_f
      ));
  }

  // simple linear regression for A
  cl_event e_slope_a;
  {
    E(clSetKernelArg(k_regression, 0, sizeof(num_a), &num_a));
    E(clSetKernelArg(k_regression, 1, sizeof(as), &as));
    size_t global = q_resolution + 1;
    E(clEnqueueNDRangeKernel
      ( queue, k_regression, 1, NULL, &global, NULL
      , levels, e_vertical, &e_slope_a
      ));
  }

  // compute D
  cl_event e_dim_q;
  {
    E(clSetKernelArg(k_dim_q, 0, sizeof(fs), &fs));
    E(clSetKernelArg(k_dim_q, 1, sizeof(as), &as));
    E(clSetKernelArg(k_dim_q, 2, sizeof(ds), &ds));
    E(clSetKernelArg(k_dim_q, 3, sizeof(Q_min), &Q_min));
    E(clSetKernelArg(k_dim_q, 4, sizeof(Q_scale), &Q_scale));
    size_t global = q_resolution + 1;
    cl_event waitlist[2] = { e_slope_f, e_slope_a };
    E(clEnqueueNDRangeKernel
      ( queue, k_dim_q, 1, NULL, &global, NULL
      , 2, waitlist, &e_dim_q
      ));
  }

  // read back F, A, D
  float f[q_resolution + 1][4];
  float a[q_resolution + 1][4];
  float D[q_resolution + 1][4];
  {
    size_t origin[3] = { 0, 0, 0 };
    size_t region[3] = { q_resolution + 1, 1, 1 };
    E(clEnqueueReadImage
      ( queue, fs, CL_TRUE, origin, region, 0, 0, &f[0][0]
      , 1, &e_slope_f, NULL
      ));
    E(clEnqueueReadImage
      ( queue, as, CL_TRUE, origin, region, 0, 0, &a[0][0]
      , 1, &e_slope_a, NULL
      ));
    E(clEnqueueReadImage
      ( queue, ds, CL_TRUE, origin, region, 0, 0, &D[0][0]
      , 1, &e_dim_q, NULL
      ));
  }

  // output
  {
    FILE *dat = fopen(output_filename, "wb");
    if (! dat)
    {
      abort();
    }
    for (int q = 0; q < q_resolution + 1; ++q)
    {
      float Q = Q_min + Q_scale * q;
      if (q == q_zero || q == q_one)
      {
        continue;
      }
      fprintf(dat, "%g", Q);
      fprintf(dat, "\t%g\t%g\t%g\t%g", D[q][0], D[q][1], D[q][2], D[q][3]);
      fprintf(dat, "\t%g\t%g\t%g\t%g", a[q][0], a[q][1], a[q][2], a[q][3]);
      fprintf(dat, "\t%g\t%g\t%g\t%g", f[q][0], f[q][1], f[q][2], f[q][3]);
      fprintf(dat, "\n");
    }
    fclose(dat);
  }

  // exit
  return 0;
}
