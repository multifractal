/*
    multifractal -- compute multifractal spectrum of 2D HDR images
    Copyright (C) 2020  Claude Heiland-Allen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma OPENCL EXTENSION cl_khr_fp64 : enable

constant sampler_t S
  = CLK_NORMALIZED_COORDS_FALSE
  | CLK_ADDRESS_NONE
  | CLK_FILTER_NEAREST;

float4 sanitize(float4 x)
{
  return -1.0f/0.0f < x && x < 1.0f/0.0f ? x : 0;
}

__kernel
void k_mipmap_reduce
  ( __read_only image2d_t src
  , __write_only image2d_t dst
  )
{
  int x = get_global_id(0);
  int y = get_global_id(1);
  int width = get_image_width(src);
  int height = get_image_height(src);
  float4 acc = (float4)(0, 0, 0, 0);
  int u = x << 1;
  int v = y << 1;
  int n = 0;
  acc += sanitize(read_imagef(src, S, (int2)(u, v)));
  if (u + 1 < width)
  {
    acc += sanitize(read_imagef(src, S, (int2)(u + 1, v)));
  }
  if (v + 1 < height)
  {
    acc += sanitize(read_imagef(src, S, (int2)(u, v + 1)));
    if (u + 1 < width)
    {
      acc += sanitize(read_imagef(src, S, (int2)(u + 1, v + 1)));
    }
  }
  write_imagef(dst, (int2)(x, y), acc);
}

__kernel
void k_horizontal
  ( __read_only image2d_t totals
  , __read_only image2d_t src
  , __write_only image2d_t log_sum_P_q
  , __write_only image2d_t log_neg_sum_P_q_log_P
  , const float Q_min
  , const float Q_scale
  )
{
  int q = get_global_id(0);
  double Q = Q_min + Q_scale * q;
  int y = get_global_id(1);
  float4 N = read_imagef(totals, S, (int2)(0, 0));
  int width = get_image_width(src);
  double4 sum_P_q = (double4)(0, 0, 0, 0);
  double4 sum_P_q_log_P = (double4)(0, 0, 0, 0);
  for (int x = 0; x < width; ++x)
  {
    int2 icoord = (int2)(x, y);
    double4 P =
      convert_double4(sanitize(read_imagef(src, S, icoord) / N));
    double4 P_q =
      P > 0
      ? Q == 0
        ? 1.0
        : pow(P, Q)
      : 0.0;
    double4 log_P =
      P > 0
      ? Q == 0
        ? 0.0
        : log2(P)
      : 0.0;
    sum_P_q += P_q;
    sum_P_q_log_P += P_q * log_P;
  }
  float4 out_P_q = convert_float4(log2(sum_P_q));
  float4 out_P_q_log_P = convert_float4(log2(-sum_P_q_log_P));
  int2 ocoord = (int2)(q, y);
  write_imagef(log_sum_P_q, ocoord, out_P_q);
  write_imagef(log_neg_sum_P_q_log_P, ocoord, out_P_q_log_P);
}

__kernel
void k_vertical
  ( __read_only image2d_t log_sum_P_q
  , __read_only image2d_t log_neg_sum_P_q_log_P
  , __write_only image2d_t num_f
  , __write_only image2d_t num_a
  , const float Q_min
  , const float Q_scale
  , const int level
  )
{
  int q = get_global_id(0);
  double Q = Q_min + Q_scale * q;
  int height = get_image_height(log_sum_P_q);
  double4 sum_P_q = (double4)(0, 0, 0, 0);
  double4 sum_P_q_log_P = (double4)(0, 0, 0, 0);
  for (int y = 0; y < height; ++y)
  {
    int2 icoord = (int2)(q, y);
    sum_P_q += exp2(convert_double4(read_imagef(log_sum_P_q, S, icoord)));
    sum_P_q_log_P -=
      exp2(convert_double4(read_imagef(log_neg_sum_P_q_log_P, S, icoord)));
  }
  double4 sum_P_q_log_P_q = sum_P_q_log_P * Q;
  double4 f = sum_P_q_log_P_q / sum_P_q - log2(sum_P_q);
  double4 a = sum_P_q_log_P / sum_P_q;
  float4 out_f = convert_float4(f);
  float4 out_a = convert_float4(a);
  int2 ocoord = (int2)(q, level);
  write_imagef(num_f, ocoord, out_f);
  write_imagef(num_a, ocoord, out_a);
}

__kernel
void k_regression
  ( __read_only image2d_t log_y
  , __write_only image2d_t slope
  )
{
  float large = 1.0e16;
  int q = get_global_id(0);
  int levels = get_image_height(log_y);
  float4  bar = (float4)(0, 0, 0, 0);
  float4 xbar = (float4)(0, 0, 0, 0);
  float4 ybar = (float4)(0, 0, 0, 0);
  for (int level = 0; level < levels; ++level)
  {
    int2 icoord = (int2)(q, level);
    float4 y = read_imagef(log_y, S, icoord);
    int4 ok = -large < y && y < large;
    bar += ok ? 1.0f : 0.0f;
    xbar += ok ? level : 0.0f;
    ybar += ok ? y : 0.0f;
  }
  xbar /= bar;
  ybar /= bar;
  float4 num = (float4)(0, 0, 0, 0);
  float4 den = (float4)(0, 0, 0, 0);
  for (int level = 0; level < levels; ++level)
  {
    int2 icoord = (int2)(q, level);
    float4 y = read_imagef(log_y, S, icoord);
    int4 ok = -large < y && y < large;
    float4 dx = ok ? level - xbar : 0.0f;
    float4 dy = ok ? y - ybar : 0.0f;
    num += dx * dy;
    den += dx * dx;
  }
  float4 r = num / den;
  write_imagef(slope, (int2)(q, 0), r);
}

__kernel
void k_dim_q
  ( __read_only image2d_t f_q
  , __read_only image2d_t a_q
  , __write_only image2d_t D_q
  , const float Q_min
  , const float Q_scale
  )
{
  int q = get_global_id(0);
  float Q = Q_min + Q_scale * q;
  int2 coord = (int2)(q, 0);
  float4 f = read_imagef(f_q, S, coord);
  float4 a = read_imagef(a_q, S, coord);
  float4 D = (a * Q - f) / (Q - 1);
  write_imagef(D_q, coord, D);
}
