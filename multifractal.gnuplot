#
#   multifractal -- compute multifractal spectrum of 2D HDR images
#   Copyright (C) 2020  Claude Heiland-Allen
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# usage: gnuplot -c multifractal.gnuplot input.dat q-dq.png q-dq-excerpt.png a-fa.png
unset key
unset title
set samples 1000
set grid
set term pngcairo enhanced size 788,576
set output ARG2
set xlabel "Q"
set ylabel "D(Q)"
plot [-10:10] [0:5] ARG1 u 1:2 w l lc rgb 0xff0000, '' u 1:3 w l lc rgb 0x00ff00, '' u 1:4 w l lc rgb 0x0000ff, '' u 1:5 w l lc rgb 0x000000
set output ARG3
plot [0:2] [0:2] ARG1 u 1:2 w l lc rgb 0xff0000, '' u 1:3 w l lc rgb 0x00ff00, '' u 1:4 w l lc rgb 0x0000ff, '' u 1:5 w l lc rgb 0x000000
set output ARG4
set xlabel "a"
set ylabel "f"
plot [0:5] [0:2] ARG1 u 6:10 w l lc rgb 0xff0000, '' u 7:11 w l lc rgb 0x00ff00, '' u 8:12 w l lc rgb 0x0000ff, '' u 9:13 w l lc rgb 0x000000
