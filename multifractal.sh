#!/bin/bash
#
#   multifractal -- compute multifractal spectrum of 2D HDR images
#   Copyright (C) 2020  Claude Heiland-Allen
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
for flame in *.flame
do
  stem="$(basename "${flame%.flame}")"
  if [[ ! -e "${stem}.rendered" ]]
  then
    emberrender \
      --in="${flame}" --name_enable --prefix="${stem}_" \
      --supersample=1 --quality 2000 --sbpctth=1 \
      --sp --opencl --device=0 --verbose \
      --format exr32 --raw_histogram --transparency
    touch "${stem}.rendered"
  fi
done
for exr in *.exr
do
  echo "EXR: ${exr}"
  stem="$(basename "${exr%.exr}")"
  if [[ ! -e "${stem}.dat" ]]
  then
    time ../multifractal 0 1280 "${exr}" "${stem}.dat"
    gnuplot -c ../multifractal.gnuplot "${stem}.dat" \
      "${stem}_q-dq.png" "${stem}_q-dq-excerpt.png" "${stem}_a-fa.png"
  fi
done
